//
//  AstronautView.swift
//  MoonShot
//
//  Created by Leadconsultant on 11/27/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

struct AstronautView: View {
    let astronaut : Austronaut
    var body: some View {
        GeometryReader{ geometry in
            ScrollView(.vertical){
                VStack{
                Image(self.astronaut.id)
                .resizable()
                    .scaledToFit()
                    .frame(width: geometry.size.width)
                    
                    Text(self.astronaut.description)
                    .padding()
                
                
            }
        }
            
            
        }.navigationBarTitle(Text(astronaut.name), displayMode: .inline)
        
        
    }
}

struct AstronautView_Previews: PreviewProvider {
    static let astronauts: [Austronaut] = Bundle.main.decode("astronauts.json")
    static var previews: some View {
        AstronautView(astronaut: astronauts[0])
    }
}
