//
//  Bundle-Decodable.swift
//  MoonShot
//
//  Created by Leadconsultant on 11/26/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import Foundation

extension Bundle{
    func decode< T: Codable > (_ file:String) -> T {
        //get file URL
        guard let url = self.url(forResource: file, withExtension: nil) else{
            fatalError("Failed to get the URL for the resource")
        }
         //create Data
        guard let data = try? Data(contentsOf: url) else {
            fatalError("Failed to read from the resource")
        }
        let decoder = JSONDecoder()
        let formatter = DateFormatter()
        formatter.dateFormat = "y-MM-dd"
        decoder.dateDecodingStrategy = .formatted(formatter)
        
          //decode data
        guard let loaded = try? decoder.decode( T.self, from: data) else {
             fatalError("Failed to decode")
        }

        return loaded
    }
    
    
    
    
}
