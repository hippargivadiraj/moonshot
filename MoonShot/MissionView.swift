//
//  MissionView.swift
//  MoonShot
//
//  Created by Leadconsultant on 11/26/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

struct MissionView: View {
    
    struct CrewMember {
        let role: String
        let astronaut: Austronaut
    }
    var astronauts : [CrewMember]
    
    init(mission:Mission, astronaut:[Austronaut]) {
        self.mission = mission
        
        var matches = [CrewMember]()
        
        for member in mission.crew {
            if let match = astronaut.first(where: { $0.id == member.name}){
                matches.append(CrewMember(role: member.role, astronaut: match))
            }else{
                fatalError("Missing \(member)")
            }
        }
            
            self.astronauts = matches
        }
    
    
    let  mission: Mission
    var body: some View {
        GeometryReader{ geometry in
            ScrollView() {
                VStack {
                    Image(self.mission.image)
                    .resizable()
                    .scaledToFit()
                        .frame(width: geometry.size.width * 0.7)
                        .padding(.top)
                    Text(self.mission.description)
                    .padding()
                    
                    ForEach(self.astronauts, id:\.role ){ crewMember in
                        
                        NavigationLink(destination: AstronautView(astronaut: crewMember.astronaut)){
                        HStack{
                            Image(crewMember.astronaut.id)
                            .resizable()
                            .frame(width: 83, height: 60 )
                            .clipShape(Capsule())
                                .overlay(Capsule().stroke(Color.primary, lineWidth: 1 ))
                            
                            VStack(alignment: .leading){
                                Text(crewMember.astronaut.name)
                                    .font(.headline)
                                Text(crewMember.role)
                                    .foregroundColor(.secondary)
                                
                            }
                            
                        }.padding(.horizontal)
                        }.buttonStyle(PlainButtonStyle())
                    }
                
                    
                Spacer()
                }
            }.padding(.horizontal)
            
        }
    }
}


struct MissionView_Previews: PreviewProvider {
    static let missions: [Mission] = Bundle.main.decode("missions.json")
    static let astonauts: [Austronaut] = Bundle.main.decode("astronauts.json")
    static var previews: some View {
        MissionView(mission: missions[0], astronaut: astonauts)
    }
}
