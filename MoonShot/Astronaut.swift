//
//  Astronaut.swift
//  MoonShot
//
//  Created by Leadconsultant on 11/26/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import Foundation

struct Austronaut: Codable, Identifiable {
    let id: String
    let name:String
    let description: String
}
